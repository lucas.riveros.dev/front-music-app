import { Store } from '@ngrx/store';
import { Subscription } from 'rxjs';
import { Router } from '@angular/router';
import { Location } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../services/auth.service';

interface AppState {
  counter: number,
  currentAlbum: string
}

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss']
})

export class NavbarComponent implements OnInit {

  public counter: number;
  public currentAlbum: string;
  public checked: boolean = false;
  private storeSubscription: Subscription;

  constructor(
    public auth: AuthService,
    private location: Location,
    private router: Router,
    private store: Store<AppState>
  ) { }

  public hide() {
    if (this.checked) {
      this.checked = false;
    }
  }

  public backClick() {
    this.hide();
    if (this.router.url !== '/home' && this.router.url !== '/') {
      this.location.back();
    }
  }

  ngOnInit() {
    this.storeSubscription = this.store
      .subscribe(state => {
        this.counter = state.counter;
      })
  }

  ngOnDestroy() {
    this.storeSubscription.unsubscribe();
  }
}
