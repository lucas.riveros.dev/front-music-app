import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './components/home/home.component';
import { SearchComponent } from './components/search/search.component';
import { ArtistsComponent } from './components/artists/artists.component';
import { AlbumComponent } from './components/album/album.component';
import { CallbackComponent } from './components/callback/callback.component';
import { AuthGuard } from './services/auth.guard';


const routes: Routes = [
  { 
    path: 'player/:id',
    loadChildren: './modules/player/player.module#PlayerModule'
  },
  { path: 'home', component: HomeComponent},
  { path: 'search', component: SearchComponent, canActivate: [ AuthGuard ]},
  { path: 'artist/:id', component: ArtistsComponent},
  { path: 'album/:id', component: AlbumComponent},
  { path: 'callback', component: CallbackComponent},
  { path: '', pathMatch: 'full', redirectTo: 'home'},
  { path: '**', pathMatch: 'full', redirectTo: 'home'}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
