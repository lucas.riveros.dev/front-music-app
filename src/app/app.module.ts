import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';
import { ServiceWorkerModule } from '@angular/service-worker';
import { HTTP_INTERCEPTORS } from '@angular/common/http';

import { AppRoutingModule } from './app-routing.module';
import { StoreModule } from '@ngrx/store';
import { StoreDevtoolsModule } from '@ngrx/store-devtools';

import { AppComponent } from './app.component';
import { ImgPipe } from './shared/pipes/img.pipe';
import { environment } from '../environments/environment';
import { CardComponent } from './shared/card/card.component';
import { HomeComponent } from './components/home/home.component';
import { NavbarComponent } from './shared/navbar/navbar.component';
import { AlbumComponent } from './components/album/album.component';
import { LoadingComponent } from './shared/loading/loading.component';
import { SearchComponent } from './components/search/search.component';
import { ArtistsComponent } from './components/artists/artists.component';
import { counterReducer } from './components/search/search-counter.reducer';
import { CallbackComponent } from './components/callback/callback.component';
import { currentAlbumReducer } from './components/album/current-album.reducer';
import { TokenInterceptorService } from './interceptors/token-interceptor.service';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    SearchComponent,
    ArtistsComponent,
    NavbarComponent,
    CardComponent,
    ImgPipe,
    LoadingComponent,
    AlbumComponent,
    CallbackComponent
  ],
  imports: [
    StoreModule.forRoot({ 
      counter: counterReducer,
      currentAlbum :currentAlbumReducer
    }),
    StoreDevtoolsModule.instrument({
      maxAge: 25,
      logOnly: environment.production
    }),
    BrowserModule,
    FormsModule,
    AppRoutingModule,
    HttpClientModule,
    ServiceWorkerModule.register('ngsw-worker.js', { enabled: environment.production })
  ],
  providers: [
    {
      provide: HTTP_INTERCEPTORS,
      useClass: TokenInterceptorService,
      multi: true
    }
  ],
  exports: [LoadingComponent],
  bootstrap: [AppComponent]
})
export class AppModule { }
