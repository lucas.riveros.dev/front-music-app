import { Action } from '@ngrx/store';
import { INCREASE } from './search-counter.action';

export function counterReducer(state: number = 0, action: Action) {

  switch (action.type) {

    case INCREASE:
      return state + 1;

    default:
      return state;
  }
}
