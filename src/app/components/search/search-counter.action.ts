import { Action } from '@ngrx/store';

export const INCREASE = '[Counter] Increase';

export class IncreaseAction implements Action {
  readonly type = INCREASE;
}
