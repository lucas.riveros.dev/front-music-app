import { Router } from '@angular/router'
import { Component } from '@angular/core';
import { AuthService } from '../../services/auth.service';
import { SpotifyService } from 'src/app/services/spotify.service';
import { IncreaseAction } from './search-counter.action';
import { Store } from '@ngrx/store';

interface AppState {
  counter: number
}

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.scss']
})

export class SearchComponent {
  
  artists: any[];
  loading: boolean;
  loaded: boolean;

  constructor ( 
    public auth: AuthService,
    private _router: Router,
    private store: Store<AppState>,
    private spotify: SpotifyService
  ) {}

  public searchArtist(e: KeyboardEvent, term: string) {
    if (e.key === "Enter" && term) {
      this.loading = true;
      this.spotify.getArtists(term).subscribe( (val: any) => {
        this.artists = val;
        this.loading = false;
        this.loaded = true;
      })
      const action = new IncreaseAction();
      this.store.dispatch( action );
    }
  }

  public goArtist(artistId: string) {
    this._router.navigate(['/artist', artistId])
  }
}
