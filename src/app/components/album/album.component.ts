import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { SpotifyService } from 'src/app/services/spotify.service';
import { Store } from '@ngrx/store';
import { SetAlbumAction } from './current-album.action'
import { Subject } from 'rxjs/internal/Subject';
import { takeUntil } from 'rxjs/operators';

interface AppState {
  currentAlbum: string
}

@Component({
  selector: 'app-album',
  templateUrl: './album.component.html',
  styleUrls: ['./album.component.scss']
})
export class AlbumComponent implements OnInit, OnDestroy {

  tracks: any[];
  loading: boolean = true;
  imgUrl: string;
  private albumId: string;
  private unsubscribe$ = new Subject<void>();

  constructor(
    private store: Store<AppState>,
    private _activatedRoute: ActivatedRoute,
    private spotify: SpotifyService
  ) { }

  ngOnInit() {
    this._activatedRoute.params
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe(params => {
        this.albumId = params.id;
        this.imgUrl = params.imgUrl;
        window.scrollTo(0, 0);
        const action = new SetAlbumAction(this.albumId);
        this.store.dispatch(action);
      })

    this.spotify.getTracks(this.albumId)
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe(val => {
        this.tracks = val;
        this.loading = false;
      });
  }

  ngOnDestroy() {
    this.unsubscribe$.next();
    this.unsubscribe$.complete();
  }
}