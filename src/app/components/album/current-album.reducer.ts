import { SETALBUM, actions } from './current-album.action';

export function currentAlbumReducer(state: string = '', action: actions) {

  switch (action.type) {

    case SETALBUM:
      return action.payload;

    default:
      return state;
  }
}
