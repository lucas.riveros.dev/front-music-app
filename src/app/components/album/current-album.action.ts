import { Action } from '@ngrx/store';

export const SETALBUM = '[CurrentAlbum] SetAlbum';

export class SetAlbumAction implements Action {
  readonly type = SETALBUM;

  constructor(public payload: string) { }
}

export type actions = SetAlbumAction;