import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http'
import { map } from 'rxjs/operators'
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})

export class SpotifyService {  

  constructor (private http: HttpClient) {}

  getToken() {
    return this.http.get(`/spotify/${environment.spotify.clientId}/${environment.spotify.clientSecret}`);
  }

  getQuery(query: string) {
    const url = `https://api.spotify.com/v1/${query}`;
    return this.http.get(url);
  }

  getNewReleases() {
    return this.getQuery(`browse/new-releases`)
      .pipe(map(val => val['albums'].items));
  }

  getArtists(term){
    return this.getQuery(`search?q=${term}&type=artist&limit=20`)
      .pipe(map(val => val['artists'].items))
  }

  getAlbums(artistId){
    return this.getQuery(`artists/${artistId}/albums`)
    .pipe(map(val => val['items']))
  }

  getTracks(albumId){
    return this.getQuery(`albums/${albumId}/tracks`)
      .pipe(map(val => val['items']))
  }
}