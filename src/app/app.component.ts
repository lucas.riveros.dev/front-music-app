import { Component } from '@angular/core';
import { SpotifyService } from './services/spotify.service';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'musicApp';
  spotifyServiceSubscription: Subscription;

  constructor(private spotify: SpotifyService) { }

  ngOnInit() {
    this.spotifyServiceSubscription = this.spotify.getToken()
      .subscribe((res: any) => {
        localStorage.setItem('token', res.access_token);
      });
  }

  ngOnDestroy() {
    this.spotifyServiceSubscription.unsubscribe();
  }
}
